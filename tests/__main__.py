import argparse

def main():
  args = parse_args()
  run_tests(args)


def parse_args():
  test_parser = argparse.ArgumentParser("Run the Bidon test suite.")
  test_parser.add_argument("-f", "--fail-fast", default=False, action="store_true")
  test_parser.add_argument("-v", "--verbose", default=1, action="count")
  test_parser.add_argument("-d", "--database-name", default="pypgq_test")
  test_parser.add_argument("-p", "--database-port", default=5432)
  test_parser.add_argument("-l", "--log", default=False, action="store_true")
  test_parser.add_argument("test_name", default=None, nargs="?")
  return test_parser.parse_args()


def run_tests(args):
  import tests
  import unittest

  tests.configure(database_name=args.database_name, port=args.database_port)

  if args.log:
    import logging
    logger = logging.getLogger("pypgq")
    logger.setLevel(logging.DEBUG)
    logger.propagate = False
    handler = logging.StreamHandler()
    handler.setLevel(logging.DEBUG)
    logger.addHandler(handler)

    # sql_logger = logging.getLogger("bidon.db.access")
    # sql_logger.setLevel(logging.DEBUG)
    # sql_logger.propagate = False
    # sql_handler = logging.StreamHandler()
    # sql_handler.setLevel(logging.DEBUG)
    # sql_logger.addHandler(sql_handler)

  if args.test_name:
    suite = unittest.TestLoader().loadTestsFromName(args.test_name, module=tests)
  else:
    suite = unittest.TestLoader().loadTestsFromModule(tests)

  unittest.TextTestRunner(verbosity=args.verbose, failfast=args.fail_fast).run(suite)


if __name__ == "__main__":
  main()
