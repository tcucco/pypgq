"""This is a sample file of how to use the pypgq.__main__ to run a job queue."""
import logging


def setup_jobs(queue, args):
  """This method is called by pypgq.__main__

  :param queue: the job queue that will be run
  :param args: a string passed in on the command line for setup jobs
  """
  setup_logging()
  queue.add_handler("echo", echo_job)
  queue.add_handler("math", math_job)


def setup_logging():
  """Configure pypgq logging."""
  logger = logging.getLogger("pypgq")
  logger.setLevel(logging.DEBUG)
  logger.propagate= False
  handler = logging.StreamHandler()
  handler.setLevel(logging.DEBUG)
  logger.addHandler(handler)


def echo_job(data):
  log("echo job: {}".format(data["message"]))


def math_job(data):
  lhs = float(data["lhs"])
  rhs = float(data["rhs"])
  op = data["op"]
  result = None

  if op == "+":
    result = lhs + rhs
  if op == "-":
    result = lhs - rhs
  if op in ("*", "x"):
    result = lhs * rhs
  if op == "/":
    result = lhs / rhs

  log("math job: {} {} {} = {}".format(lhs, op, rhs, result))


def log(msg):
  logging.getLogger("pypgq").info(msg)
